from django import forms
from .models import *
from django.core.exceptions import ValidationError

class RecrouteForm(forms.ModelForm):
    class Meta:
        model = Recroute
        exclude = ['score', 'mentor']

    def save(self):
        new_recroute = Recroute.objects.create(
            name=self.cleaned_data['name'],
            planet=self.cleaned_data['planet'],
            age=self.cleaned_data['age'],
            email=self.cleaned_data['email'],
            score=0,
        )
        return new_recroute
