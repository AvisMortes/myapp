# Generated by Django 2.2.4 on 2019-08-06 10:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mySite', '0002_auto_20190806_0900'),
    ]

    operations = [
        migrations.CreateModel(
            name='Questions',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('orden_code', models.IntegerField(unique=True)),
                ('quest_1', models.TextField(max_length=300)),
                ('answer_1', models.BooleanField()),
                ('quest_2', models.TextField(max_length=300)),
                ('answer_2', models.BooleanField()),
                ('quest_3', models.TextField(max_length=300)),
                ('answer_3', models.BooleanField()),
            ],
        ),
        migrations.AddField(
            model_name='recroute',
            name='score',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
    ]
