from django.db import models

# Create your models here.
class Recroute(models.Model):
    name = models.CharField(max_length=50)
    planet = models.ForeignKey('Planet', on_delete=models.CASCADE)
    age = models.IntegerField()
    email = models.EmailField(unique=True)
    score = models.IntegerField()
    mentor = models.ForeignKey('Sith', null=True, on_delete=models.CASCADE)
    def __str__(self):
        return self.name

class Sith(models.Model):
    name = models.CharField(max_length=50)
    planet = models.ForeignKey('Planet', on_delete=models.CASCADE)
    def __str__(self):
        return self.name

class Planet(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Question(models.Model):
    question_text = models.CharField(max_length=100)
    def __str__(self):
        return self.question_text

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.BooleanField()

    def __str__(self):
        return self.choice_text

class Orden(models.Model):
    orden_index = models.IntegerField(unique=True)
    questions = models.ManyToManyField(Question)

    def __str__(self):
        return str(self.orden_index)




