from django.urls import path
from .views import *

urlpatterns = [
    path('', select_form, name='select_url'),
    path('recroutes/', RecrouteCreate.as_view(), name='begin_url'),
    path('create/', RecrouteCreate.as_view(), name='questions_url'),
    path(r'^(?P<question_id>[0-9]+)/(?P<email>[0-9]+)/vote/$', vote, name='vote'),
    path('siths/', sith, name='sith_url'),
    path(r'^(?P<sith_id>[0-9]+)/$', recroute_list, name='list_url'),
    path(r'^(?P<sith_id>[0-9]+)/(?P<email>[0-9]+)/message/$', message, name='message_url'),
]