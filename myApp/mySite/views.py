from django.shortcuts import render, get_object_or_404
from django.http import HttpRequest, HttpResponseRedirect, HttpResponse
from .forms import *
from django.views.generic import View
from django.urls import reverse
from django.shortcuts import redirect
from django.views import generic
from django.core.mail import send_mail

# Create your views here.
def vote(request, question_id, email):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
        anwser = Choice.objects.get(choice_text=selected_choice)
        print(anwser)
        if anwser.votes is True:
            recroute = Recroute.objects.get(email=email)
            recroute.score += 1
            recroute.save()
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'mySite/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        if(len(Question.objects.all()) >= question.id + 1):
            quest = Question.objects.get(pk=question.id + 1)
            print(anwser.votes)
            return render(request, 'mySite/quest.html', context={'email': email,
                                                                 'question': quest})
        else:
            recroute = Recroute.objects.get(email=email)
            return HttpResponse("С вами свяжутся в ближайшее время")


def recroute_list(request, sith_id):
    sith = get_object_or_404(Sith, pk=sith_id)
    selected_recroutes = Recroute.objects.filter(planet=sith.planet, mentor=None)
    return render(request, 'mySite/all_recroutes.html', context={'recroutes': selected_recroutes, 'sith': sith})


def sith(request):
    siths = Sith.objects.all()
    return render(request, 'mySite/siths.html', context={'siths': siths})


def message(request, sith_id, email):
    recroute = Recroute.objects.get(email=email)
    recroute.mentor = Sith.objects.get(pk=sith_id)
    recroute.save()
    print(email)
    send_mail('Рука тени', 'Вы приняты, теперь вы один из рук Тени.', 'avismortes1@yandex.ru',
              [email], fail_silently=False)
    return HttpResponse("Рекрут добавлен к вам в ученики")

class RecrouteCreate(View):
    def get(self, request):
        form = RecrouteForm()
        return render(request, 'mySite/create.html', context={'form': form})
    def post(self, request):
        bound_form = RecrouteForm(request.POST)
        if bound_form.is_valid():
            new_form = bound_form.save()
            quest = Question.objects.get(pk=1)
            return render(request, 'mySite/quest.html', context={'email': bound_form.cleaned_data['email'],
                                                                 'question': quest})
        return render(request, 'mySite/create.html', context={'form': bound_form})


def select_form(request):
    return render(request, 'mySite/select.html')